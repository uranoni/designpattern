## git flow 介紹
![](../pic/gitflow.png)
### Master 分支
* 用來放穩定且隨時可上線的版本。
* 工程師不會直接 Commit 到這個分支。因為是穩定版本
* 通常也會在這個分支上的 Commit 上打上版本號的tag。

### Develop 分支
* 這個分支主要是所有開發的基礎分支
* 當要新增功能的時,所有的 Feature 分支都是從這個分支切出去的。
* Feature功能完成後，也都會合併回來這個分支。

### Hotfix 分支
* 產品出現bug的時候，會從 Master 分支開一個 Hotfix 分支出來進行修復
* Hotfix 分支修復完成之後，會合併回 Master 分支
* Hotfix 分支修復完成之後也同時會合併一份到 Develop 分支。

### Release 分支
* 當認為 Develop 分支夠成熟了，就可以把 Develop 分支合併到 Release 分支，在這邊進行算是上線前的最後測試。

### Feature 分支
* 當要開始新增功能的時候，就是使用 Feature 分支的時候了。
* Feature 分支都是從 Develop 分支來的，完成之後會再併回 Develop 分支。