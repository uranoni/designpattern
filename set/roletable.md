## Our role table responsibility
## example 
|Name   |   PM |  SA| SAr|UI/UX|RD-F|RD-B|
|-------|:----:|----|----|-----|----|----|
| Roni  |   O  |    |    |  O  |    |    |
| Kevin |      |    |    |     |  O |  O |

## Follw above the table:
Roni have two role : PM , UI/UX design
Kevin is full-stack

## Role description

### PM (Project Manager)
* 專案建置
* 客戶會談
* 職責分配
### SA(System Analyst)

 * 分析系統
 * 系統測試

### SAr(System Architect)
* 系統架構建置
* 資料格式定義
### UI/UX(UIUX Engineer)
* 使用者體驗分析
* UI設計

### RD-F("R"esearch and "D"evelopment engineer - frontend side)
* 前端工程師
* 前端相關技術研究


### RD-B("R"esearch and "D"evelopment engineer - backend side)
* 後端工程師
* 後端技術研究