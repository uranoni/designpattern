## vue設計規範參照至官方guide 

### Multi-word component names
<span style="color:red">Bad</span>

容易與html tag 相衝

```javascript
    Vue.component('todo', {
  // ...
    })
export default {
  name: 'Todo',
  // ...
}
```

<span style="color:green">good</span>

有意義的命名tag可以有效分類族群
```javascript
Vue.component('todo-item', {
  // ...
})
export default {
  name: 'TodoItem',
  // ...
}
```