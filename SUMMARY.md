# Summary
## 專案開發配置
 * [introduction](README.md)

 * [role assignment](./set/roletable.md)

 * [版本控制Git flow](./set/gitflow.md)

 * [GraphQL style](./set/graphstyle.md)
## 前端
 * [前端設計框架&資源](./dev/frontenv.md)

 * [Vue設計風格](./dev/vuestyle.md)

## 後端


## 測試



