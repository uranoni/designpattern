# Introduction

專案建置流程須遵守以下規範

## 成員名單
|Name        | Git page (hub/lab...)         | Homepage  |
| ------ |-------------| -----|
|  NS Chou  | none | none |
| Roni     | https://github.com/uranoni/ | none |
| Kevin      |https://github.com/kevinypfan    |   none |
| Kao |    none  | none   |
| Daniel  |    none  |  none  |
| Malong  |    none  | none   |

## Project Start
### 專案生命週期
![](pic/projectlife.png)
[專案週期連結](set/ProjectLife.md)

## Role Assignments
### 任務分配表
### example
|Name   |   PM |  SA| SAr|UI/UX|RD-F|RD-B|
|-------|:----:|----|----|-----|----|----|
| Roni  |   O  |    |    |  O  |    |    |
| Kevin |      |    |    |     |  O |  O |
[任務腳色分配表連結](set/roletable.md)

## Git flow
![](pic/gitflow.png)
[gitflow連結](set/gitflow.md)





